// cargar el modulo de express
const express = require("express");
// y crea una instancia de la aplicación express
const app = express();
// cargar body parser para leer el body de los request
const bodyParser = require("body-parser");

// recibir datos en formato json
app.use(bodyParser.json());
 
//simulación base de datos
var usuario1 = {id : "1",
                nombre: "Alba",
                apellido: "Chinchilla"
                };

var usuario2 = {id : "2",
                nombre: "Josue",
                apellido: "Chinchilla"
                };                

bdusuarios = [usuario1,usuario2];

/*
    Esta funcion devuelve el id de un cliente
    si lo encuentra, de lo contrario retorna -1
    parametros
    nombre y apellido del cliente 
*/
app.get("/getId/:nombre/:apellido",(req,res) =>{
    var nombreCliente = req.params.nombre;
    var apellidoCliente = req.params.apellido
    for(var i = 0; i <bdusuarios.length; i++){
        if(bdusuarios[i].nombre === nombreCliente && bdusuarios[i].apellido === apellidoCliente){
            res.status(200).send(bdusuarios[i].id);
            return;
        }
    }
    res.send("-1");
})
app.get("/pedirViaje/:ubicacion", (req,res) =>{
    var ubicacionCliente = req.params.ubicacion;
    //Utilizando Rastreo debe ver si hay un Piloto Cerca
    axios.get("http://localhost:4548/").then((response) =>{
        alert("Hay un Piloto cerca, confirmas tu viaje?")
    })
    //1. Enviar Ubicacion del cliente
    //2. Esperar confirmación de viaje
    //3. Mostrar Ubicación del Uber
    //4. Confirmar?
})
app.get('/', (req, res) =>{
    res.send("Ruta principal");
})

app.get('/pilotos/:id',(req,res) =>{
    res.send(req.params.id);
})

app.post("/usuario", (req, res) =>{
    console.log(req.body)
    res.send("Ruta post usuario");
})

var server = app.listen(4545, () => {
    console.log("Corriendo servicio de usuario");
});

module.exports = server;