var Request = require("request");

describe("Server", ()=>{
    var server;
    beforeAll(() =>{
        server = require("../usuario");
    })
    afterAll(() =>{
        server.close();
    })
    describe("GET /", () =>{
        var data = {};
        beforeAll((done) =>{
            var nombre = 'Alba';
            var apellido = 'Chinchilla';
            Request.get("http://localhost:4545/getId/"+nombre+"/"+apellido+"", (error,response,body) =>{
                data.status = response.statusCode;
                data.body = body;
                done();
            });
        });
        //Prueba para ver el estado obtenido de la solicitud
        it("Status 200",() =>{
            expect(data.status).toBe(200);
        })
        
        //prueba para resultado correcto
        it("Body 1",() =>{
            expect(data.body).toBe("1");
        })
        
    })
});