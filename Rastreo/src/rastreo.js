// cargar el modulo de express
const express = require("express");
// y crea una instancia de la aplicación express
const app = express();
// cargar body parser para leer el body de los request
const bodyParser = require("body-parser");
const axios = require("axios")
// recibir datos en formato json
app.use(bodyParser.json());

//Simulando la base de datos
//informacion de pilotos
var rastro_piloto1 = {id: 1, disponible: 1, ubicacion: 1234};
var rastro_piloto2 = {id: 2, disponible: 0, ubicacion: 5555};
// informacion de clientes
var rastro_cliente1 = {id: 1, ubicacion: 1234};
var rastro_cliente2 = {id: 2, ubicacion: 5555};
var rastro_pilotos = [rastro_piloto1, rastro_piloto2];
var rastro_clientes = [rastro_cliente1, rastro_cliente2];

/*
    Retorna la ubicación de un cliente,
    ya que entra como parametro su id.
*/

app.get('/getUbicacionCliente/:id_cliente',(req,res) =>{
    console.log("getUbicacionCliente");
    var id_cliente = req.params.id_cliente;
    for(var i =0; i < rastro_clientes.length; i++){
        if(rastro_clientes[i].id == id_cliente){
            res.send(String(rastro_clientes[i].ubicacion));
        }
    }
})
/*
    Funcion que retorna el id de un piloto cercano a la ubicacion
    que entra como parametro.
*/
app.get('/getPilotoCercano/:ubicacion',(req,res) =>{
    var ubicacion = parseInt(req.params.ubicacion,10);
    for(i = 0; i < rastro_pilotos.length; i++){
        if(rastro_pilotos[i].ubicacion>= (ubicacion-5) &&
            rastro_pilotos[i].ubicacion<= (ubicacion +5) ){
                res.send(String(rastro_pilotos[i].id));
            }
    }

})


app.get('/', (req, res) =>{
    res.send("Ruta principal rastreo");
})

app.get("/atencion_usuario/:ubicacionCliente",(req,res) =>{
    
})

app.listen(4544, () => {
    console.log("Corriento servicio de rastreo");
});

