const express = require("express")
const app = express()
const bodyParser = require("body-parser")
const axios = require("axios")
app.use(bodyParser.json())

const microservicioUsuario = "http://localhost:4545";
const microservicioPiloto = "http://localhost:4546";
const microservicioRastreo = "http://localhost:4544";

function getIdFuncion(nombre, apellido){
    var ruta = microservicioUsuario+"/getId/"+nombre+"/"+apellido;
     try{
            return axios.get(ruta).then((response) => {
            var id_Usuario = String(response.data);
            return id_Usuario;  
            }
            )
        }catch(error){
            return error;
        }
}
function ubicarCliente(id){
    var ruta = microservicioRastreo+"/getUbicacionCliente/"+id;
    try{
        return axios.get(ruta).then((response) => {
        var ubicacion_Usuario = String(response.data);
        return ubicacion_Usuario;  
        }
        )
    }catch(error){
        return error;
    }
}
function getPiloto(ubicacionCercana){
    var ruta = microservicioRastreo+"/getPilotoCercano/"+ubicacionCercana;
    try{
        return axios.get(ruta).then((response) =>{
            var idPiloto = String(response.data);
            return idPiloto;
        })
    }catch(error){
        return error;
    }
}
function infPiloto(idPiloto){
    var ruta = microservicioPiloto + "/piloto/"+idPiloto;
    try{
        return axios.get(ruta).then((response) =>{
            var info = String(response.data);
            return info;
        })
    }catch(error){
        return error;
    }
}
app.get("/pedirUber/:nombre/:apellido",(req,res) =>{
    /*
    .   Funcion que recibe como parametro nombre y apellido 
        de un cliente y hace la solicitud al microservicio
        del clientes con estos parametros.
    */
    var nombre = req.params.nombre;
    var apellido = req.params.apellido;
    getIdFuncion(nombre,apellido).then(data => {
        var id_cliente = data;
        console.log("El id del cliente "+id_cliente);
        ubicarCliente(id_cliente).then(data1 =>{
            var ubicacion_cliente = data1;
            console.log("La ubicacion del cliente: "+data1);
            getPiloto(ubicacion_cliente).then(data2 =>{
                var id_piloto = data2;
                console.log("Id piloto cercano: "+data2);
                infPiloto(id_piloto).then(data3 =>{
                    var informacion = data3;
                    res.send("Tu Uber esta llegando, y es: "+informacion);
                })

            })
        })
    })
   // res.send(":D");
})


app.listen(4548, () => {
    console.log("Corriento servicio de ESB");
});