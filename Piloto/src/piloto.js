// cargar el modulo de express
const express = require("express");
// y crea una instancia de la aplicación express
const app = express();
// cargar body parser para leer el body de los request
const bodyParser = require("body-parser");

//VARIABLES
var piloto1 = {id: 1, nombre: "Alex Garcia", auto: "Toyota Yaris Gris P809FVJ"};
var piloto2 = {id: 2, nombre: "Sergio Sanchez", auto: "Hyundai Accent Rojo P810FVJ"};
var piloto3 = {id: 3, nombre: "Luis Ramirez", auto: "Nissan Sentra Blanco P811FVJ"};
var pilotos = [piloto1, piloto2, piloto3];

// recibir datos en formato json
app.use(bodyParser.json());

app.get("/pilotos", (req, res) =>{
    res.json(pilotos)
})

app.get('/', (req, res) =>{
    res.send("Ruta principal piloto");
})

app.get("/piloto/:id", (req, res) =>{
    var id_piloto = parseInt(req.params.id,10);
    for(var i = 0; i < pilotos.length; i++){
        if(pilotos[i].id === id_piloto  ){
                res.send(pilotos[i].nombre + " Auto: "+pilotos[i].auto);
            }
    }
})


app.listen(4546, () => {
    console.log("Corriento servicio de piloto");
});

