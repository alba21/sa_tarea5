const gulp = require('gulp');
const zip = require('gulp-zip');
var fileindex = require('gulp-fileindex');

gulp.task('artefactoPiloto',()=>
    gulp.src('Piloto/src/*')
        .pipe(zip('piloto.zip'))
        .pipe(gulp.dest('dist'))
);

gulp.task('artefactoRastreo',()=>
    gulp.src('Rastreo/src/*')
        .pipe(zip('rastreo.zip'))
        .pipe(gulp.dest('dist'))
);

gulp.task('artefactoUsuario',()=>
    gulp.src('Usuario/src/*')
        .pipe(zip('usuario.zip'))
        .pipe(gulp.dest('dist'))
);

gulp.task('artefactoESB',()=>
    gulp.src('ESB/src/*')
        .pipe(zip('esb.zip'))
        .pipe(gulp.dest('dist'))
);

gulp.task('fileindex', function() {
    return gulp.src('dist/*.zip')
      .pipe(fileindex())
      .pipe(gulp.dest('./dist'));
});

gulp.task('default', gulp.series('artefactoPiloto','artefactoRastreo', 'artefactoUsuario','artefactoESB','fileindex'));

